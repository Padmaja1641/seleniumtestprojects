package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import com.intimetec.amazon.pom.BuyHpLaptopPage;
import com.intimetec.amazon.pom.BuyKeyboardPage;
import com.intimetec.amazon.pom.DeliveryAddressPage;
import com.intimetec.amazon.pom.HPLaptopPage;
import com.intimetec.amazon.pom.HomePage;
import com.intimetec.amazon.pom.KeyboardPage;
import com.intimetec.amazon.pom.LandingPage;
import com.intimetec.amazon.pom.ShippingPage;
import com.intimetec.amazon.pom.ShoppingCartPage;
import com.intimetec.amazon.pom.SignInPage;
import com.intimetec.amazon.util.ConfigFileReader;
import com.intimetec.amazon.util.LaunchBrowser;

public class AmazonTest {

	private static final Logger logger = Logger.getLogger(AmazonTest.class);

	private ConfigFileReader config;
	private WebDriver driver;

	public AmazonTest() {
		logger.info(this.getClass().getSimpleName() + " object is created.");
	}

	@BeforeSuite
	public void setUp() {
		logger.info("Invoked setUp");
		config = LaunchBrowser.getConfig();
		driver = LaunchBrowser.launchChromeBrowser();
		logger.info("Browser is launched.");
	}
	
	@Test(priority = 1)
	public void verifyClickOnSignIn() {
		logger.info("Invoked verifyClickOnSignIn");
		LandingPage landingPage = new LandingPage(driver);
		landingPage.clickOnSignIn();
		logger.info("Clicked on SignIn button successfully.");
	}

	@Test(priority = 2)
	public void testEmailField() {
		logger.info("Invoked testEmailField");
		SignInPage signInPage = new SignInPage(driver, config);
		signInPage.enterEmailId();
		logger.info("Entered email id successfully.");
	}
	@Test(priority = 3)
	public void testContinueButton() {
		logger.info("Invoked testContinueButton.");
		SignInPage signInPage = new SignInPage(driver, config);
		signInPage.clickOnContinue();
		logger.info("Cliked on continue button successfully.");
	}
	@Test(priority = 4)
	public void testPasswordField() {
		logger.info("Invoked testPasswordField");
		SignInPage signInPage = new SignInPage(driver, config);
		signInPage.enterPassword();
		logger.info("Entered password successfully.");
	}
	@Test(priority = 5)
	public void testSignInButton() {
		logger.info("Invoked testSignInButton");
		SignInPage signInPage = new SignInPage(driver, config);
		signInPage.clickOnSignIn();
		logger.info("Able to signin successfully.");
	}
	
	@Test(priority = 6)
	public void testVerifyUserName() {
		logger.info("Invoked testVerifyUserName");
		SignInPage signInPage = new SignInPage(driver, config);
		Assert.assertTrue(signInPage.verifyUserLogin(), "Correct username displayed.");
	}
	
	@Test(priority = 7)
	public void testsearchLaptop() throws Exception {
		logger.info("Invoked testsearchLaptop");
		HomePage homePage = new HomePage(driver, config);
		homePage.searchLaptop();
		logger.info("Able to search successfully.");
	}
	
	@Test(priority = 8)
	public void testApplyFiltersForLaptop() {
		logger.info("Invoked testApplyFiltersForLaptop");
		HPLaptopPage hpLaptopPage = new HPLaptopPage(driver);
		hpLaptopPage.applyFilters();
		logger.info("Applied the filters successfully.");
	}

	@Test(priority = 9)
	public void testclickOnLaptop() {
		logger.info("Invoked testclickOnLaptop");
		HPLaptopPage hpLaptopPage = new HPLaptopPage(driver);
		hpLaptopPage.clickOnItem();
		logger.info("Cliked on the product successfully.");
	}

	@Test(priority = 10)
	public void testAddLaptopToCart() {
		logger.info("Invoked testAddLaptopToCart");
		BuyHpLaptopPage buyHpLaptopPage = new BuyHpLaptopPage(driver);
		buyHpLaptopPage.clickOnAddToCart();
		logger.info("Product has been added to cart successfully.");
	}

	@Test(priority = 11)
	public void testCancelPopUp() {
		logger.info("Invoked testCancelPopUp");
		BuyHpLaptopPage buyHpLaptopPage = new BuyHpLaptopPage(driver);
		buyHpLaptopPage.cancelPopUp();
		logger.info("Installation popup has been canceled.");
	}
	
	@Test(priority = 12)
	public void testAddToCart() {
		logger.info("Invoked testAddToCart");
		BuyHpLaptopPage buyHpLaptopPage = new BuyHpLaptopPage(driver);
		Assert.assertEquals(buyHpLaptopPage.verifyAddToCart(), "Added to Cart", "Product not added to cart.");
	}
	
	@Test(priority = 13)
	public void testGetLaptopPrice() {
		logger.info("Invoked testGetLaptopPrice");
		ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver);
		shoppingCartPage.getPrice();
		logger.info("Laptop price has been fetched.");
	}

	@Test(priority = 14)
	public void testSearchKeyboard() throws Exception {
		logger.info("Invoked testSearchKeyboard");
		HomePage homePage = new HomePage(driver, config);
		homePage.searchKeyboard();
		logger.info("Searched the product: Keyboard.");
	}

	@Test(priority = 15)
	public void testApplyFiltersForKeyboard() {
		logger.info("Invoked testApplyFiltersForKeyboard");
		KeyboardPage keyboardPage = new KeyboardPage(driver);
		keyboardPage.applyFilters();
		logger.info("Filters has been applied for Keyboard.");
	}

	@Test(priority = 16)
	public void testClickOnKeyboard() {
		logger.info("Invoked testClickOnKeyboard");
		KeyboardPage keyboardPage = new KeyboardPage(driver);
		keyboardPage.clickOnItem();
		logger.info("Clicked on the product successfully.");
	}

	@Test(priority = 17)
	public void testAddKeboardToCart() {
		logger.info("Invoked testAddKeboardToCart");
		BuyKeyboardPage buyKeyboardPage = new BuyKeyboardPage(driver);
		buyKeyboardPage.clickOnAddToCart();
		logger.info("Product has been added to cart.");
	}

	@Test(priority = 18)
	public void testGetKeyboardPrice() {
		logger.info("Invoked testGetKeyboardPrice");
		ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver);
		shoppingCartPage.getPrice();
		logger.info("Price has been fetched successfully.");
	}

	@Test(priority = 19)
	public void testIsCartUpdated() {
		logger.info("Invoked testIsCartUpdated");
		ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver);
		shoppingCartPage.isCartUpdated();
		logger.info("Cart is updated.");
	}
	
	@Test(priority = 20)
	public void testClickOnProceedToBuy() {
		logger.info("Invoked testClickOnProceedToBuy");
		ShoppingCartPage shoppingCartPage = new ShoppingCartPage(driver);
		shoppingCartPage.clickOnProceedToBuy();
		logger.info("Cliked on proceed to buy button successfully.");
	}

	@Test(priority = 21)
	public void testSelectDeliveryAddress() {
		logger.info("Invoked testSelectDeliveryAddress");
		DeliveryAddressPage deliveryAddressPage = new DeliveryAddressPage(driver);
		deliveryAddressPage.selectDeliveryAddress();
		logger.info("Selected the delivery address successfully.");
	}

	@Test(priority = 22)
	public void testClickOnContinueToPayment() {
		logger.info("Invoked testClickOnContinueToPayment");
		ShippingPage shippingPage = new ShippingPage(driver);
		shippingPage.clickOnContinue();
		logger.info("Clicked on Continue button successfully.");
	}
	
	@AfterSuite
	public void tearDown() {
		logger.info("Invoked tearDown");
		driver.quit();
		logger.info("Closed the browser.");
	}

}
