package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.intimetec.amazon.util.ConfigFileReader;

public class SignInPage {
	private static final Logger logger = Logger.getLogger(SignInPage.class);
	private WebDriver driver = null;
	private ConfigFileReader configFileReader = null;

	private By emailTextField = By.id("ap_email");
	private By continueButton = By.id("continue");
	private By password = By.id("ap_password");
	private By signInButton = By.id("signInSubmit");
	private  By loggedInUser = By.id("nav-link-accountList-nav-line-1");

	
	public SignInPage(WebDriver driver, ConfigFileReader configFileReader) {
		super();
		this.driver = driver;
		this.configFileReader = configFileReader;
		logger.info(this.getClass().getSimpleName() + " object created");
	}

	public void enterEmailId() {
		logger.info("Invoked enterEmailId");
		driver.findElement(emailTextField).sendKeys(configFileReader.getUserName());
	}

	public void clickOnContinue() {
		logger.info("Invoked clickOnContinue");
		driver.findElement(continueButton).click();
	}

	public void enterPassword() {
		logger.info("Invoked enterPassword");
		driver.findElement(password).sendKeys(configFileReader.getPassword());
	}

	public void clickOnSignIn() {
		logger.info("Invoked clickOnSignIn");
		driver.findElement(signInButton).click();
	}
	
	public boolean verifyUserLogin() {
		logger.info("verifyUserLogin");
		return driver.findElement(loggedInUser).isDisplayed();
	}

}
