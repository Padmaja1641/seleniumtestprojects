package com.intimetec.amazon.pom;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HPLaptopPage {
	private static final Logger logger = Logger.getLogger(HPLaptopPage.class);
	private WebDriver driver;
	
	private By vendor = By.xpath("//span[text() = 'HP']");
	private By memorySize = By.xpath("//li[@id = 'p_n_feature_browse-bin/1485945031']/child::span/child::a/child::div/child::label/i");
	private By hddSize = By.xpath("//span[text() = '500 - 999 GB']");
	private By hpLaptop = By.xpath("//img[@data-image-index= '1']");
	private By searchPageTitle = By.xpath("//title");
	
	public HPLaptopPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}
	
	public void applyFilters() {
		logger.info("Invoked applyFilters");
		driver.findElement(vendor).click();
		driver.findElement(memorySize).click();
		driver.findElement(hddSize).click();
	}
	
	public void clickOnItem() {
		logger.info("Invoked clickOnItem");
		driver.findElement(hpLaptop).click();
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		logger.info("Switching to next tab.");
	}
	
	public String verifySearchPage() {
		logger.info("Invoked verifySearchPage");
		return driver.findElement(searchPageTitle).getText();
	}
	
	
	
	
}
