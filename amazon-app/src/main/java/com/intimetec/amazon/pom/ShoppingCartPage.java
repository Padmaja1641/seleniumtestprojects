package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShoppingCartPage {
	private static final Logger logger = Logger.getLogger(ShoppingCartPage.class);
	private WebDriver driver;

	private static int cartPrice;
	private static int productPrice;

	private By itemPrice = By.xpath("//span[@class = 'a-color-price hlb-price a-inline-block a-text-bold'][1]");
	private By proceedToBuyButton = By.xpath("//a[@id = \'hlb-ptc-btn-native\']");

	public ShoppingCartPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}

	public void getPrice() {
		logger.info("Invoked getPrice.");
		String obtainPrice = driver.findElement(itemPrice).getText();
		productPrice = parseStringToInt(obtainPrice);
		cartPrice = cartPrice + productPrice;
		logger.info("Total price is: " + productPrice);
	}

	public void isCartUpdated() {
		logger.info("Invoked isCartUpdated.");
		if (cartPrice >= productPrice) {
			logger.info("Cart is updated.");
		} 
		
		else {
			logger.error("Cart is not updated.");
		}
	}

	public void clickOnProceedToBuy() {
		logger.info("Invoked clickOnProceedToBuy.");
		driver.findElement(proceedToBuyButton).click();
	}

	public int parseStringToInt(String productPrice) {
		productPrice = productPrice.replaceAll(",", "");
		productPrice = productPrice.replaceAll("₹.", "");
		return (int) Math.round(Double.parseDouble(productPrice));
	}

}
