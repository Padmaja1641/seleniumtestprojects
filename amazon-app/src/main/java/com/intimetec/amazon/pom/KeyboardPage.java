package com.intimetec.amazon.pom;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class KeyboardPage {
	private static final Logger logger = Logger.getLogger(KeyboardPage.class);
	private WebDriver driver;
	
	private By vendor = By.xpath("//span[text() = 'HP']");
	private By keyboard = By.xpath("//img[@data-image-index= '0']");
	
	public KeyboardPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}
	
	public void applyFilters() {
		logger.info("Invoked applyFilters");
		driver.findElement(vendor).click();
	}
	
	public void clickOnItem() {
		logger.info("Invoked clickOnItem");
		driver.findElement(keyboard).click();
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(2));
		logger.info("Switching to next tab.");
	}
}
