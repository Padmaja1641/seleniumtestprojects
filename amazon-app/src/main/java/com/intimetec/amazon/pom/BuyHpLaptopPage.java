package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BuyHpLaptopPage {
	private static final Logger logger = Logger.getLogger(BuyHpLaptopPage.class);
	private WebDriver driver;

	private By addToCartButton = By.id("add-to-cart-button");
	private By cancelInstallationPopup = By.xpath("//button[@data-action= 'a-popover-close']");
	private By addToCartTest = By.xpath("//h1");

	public BuyHpLaptopPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}

	public void clickOnAddToCart() {
		logger.info("Invoked clickOnAddToCart");
		driver.findElement(addToCartButton).click();
	}
	
	public String verifyAddToCart() {
		logger.info("Invoked verifyAddToCart");
		return driver.findElement(addToCartTest).getText();
	}
	
	public void cancelPopUp() {
		logger.info("Invoked Cancel Popup");
		try {
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(cancelInstallationPopup)).click();
			logger.info("Popup is closed.");
		}

		catch (Exception exception) {
			logger.info(exception);
			logger.error("Popup is not closed");
		}
	}
}
