package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeliveryAddressPage {
	private static final Logger logger = Logger.getLogger(DeliveryAddressPage.class);
	private WebDriver driver;

	private By selectAddress = By.xpath("//a[@data-action = \"page-spinner-show\"][1]");

	public DeliveryAddressPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}

	public void selectDeliveryAddress() {
		logger.info("Invoked selectDeliveryAddress.");
		driver.findElement(selectAddress).click();
	}
}
