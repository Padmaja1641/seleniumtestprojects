package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class BuyKeyboardPage {
	private static final Logger logger = Logger.getLogger(BuyKeyboardPage.class);
	private WebDriver driver;
	
	private By addToCartButton = By.id("add-to-cart-button");

	public BuyKeyboardPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}
	
	public void clickOnAddToCart() {
		logger.info("Invoked clickOnAddToCart");
		driver.findElement(addToCartButton).click();
	}

}
