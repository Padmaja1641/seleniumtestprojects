package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage {
	private static final Logger logger = Logger.getLogger(LandingPage.class);
	private By signInButton = By.id("nav-link-accountList");
	
	private WebDriver driver = null;
	public LandingPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() +" object is created.");
	}
	public void clickOnSignIn() {
		logger.info("Invoked clickOnSignIn");
		driver.findElement(signInButton).click();	
	}
		 
}
