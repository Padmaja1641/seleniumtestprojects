package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShippingPage {
	private static final Logger logger = Logger.getLogger(ShippingPage.class);
	private WebDriver driver;
	
	private By continueToBuyButton = By.xpath("//input[@value = \"Continue\"]");
	
	public ShippingPage(WebDriver driver) {
		super();
		this.driver = driver;
		logger.info(this.getClass().getSimpleName() + " object created");
	}
	
	public void clickOnContinue() {
		logger.info("Invoked clickOnContinue.");
		driver.findElement(continueToBuyButton).click();
	}
}
