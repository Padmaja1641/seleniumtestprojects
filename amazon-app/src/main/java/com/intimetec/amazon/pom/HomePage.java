package com.intimetec.amazon.pom;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import com.intimetec.amazon.util.ConfigFileReader;

public class HomePage {
	private static final Logger logger = Logger.getLogger(HomePage.class);
	private WebDriver driver = null;
	private ConfigFileReader configFileReader = null;
	private By SearchBox = By.id("twotabsearchtextbox");


	public HomePage(WebDriver driver, ConfigFileReader configFileReader) {
		super();
		this.driver = driver;
		this.configFileReader = configFileReader;
		logger.info(this.getClass().getSimpleName() + " object created");
	}
	
	public void searchLaptop() throws Exception {
		logger.info("Invoked searchLaptop");
		String[] ShoppingList = configFileReader.getShoppingList();
		driver.findElement(SearchBox).sendKeys(ShoppingList[0], Keys.ENTER);
	}
	
	public void searchKeyboard() throws Exception {
		logger.info("Invoked searchKeyboard");
		String[] ShoppingList = configFileReader.getShoppingList();
		driver.findElement(SearchBox).sendKeys(ShoppingList[1], Keys.ENTER);
	}
}
