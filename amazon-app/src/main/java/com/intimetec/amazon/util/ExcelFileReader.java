package com.intimetec.amazon.util;

import java.io.File;
import java.io.FileInputStream;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelFileReader {

	
	String Laptop;
	String Keyboard;
	
	public void readFromExcel() throws Exception {
	
		File file = new File("../amazon-app/src/main/resources/AmazonExcelInputOutput.xlsx");
		FileInputStream inputStream = new FileInputStream(file);
		XSSFWorkbook workBook = new XSSFWorkbook(inputStream);
		XSSFSheet sheet;
		XSSFRow row;
		XSSFCell cell;
		
		Laptop = workBook.getSheetAt(0).getRow(0).getCell(1).getStringCellValue();
		Keyboard = workBook.getSheetAt(0).getRow(1).getCell(1).getStringCellValue();
		
	}
	
	public String getLaptop() throws Exception {
		readFromExcel();
		return Laptop;
	}
	
	public String getKeyboard() throws Exception {
		readFromExcel();
		return Keyboard;
	}
	
	
	
}
