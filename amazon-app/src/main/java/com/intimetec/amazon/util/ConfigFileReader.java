package com.intimetec.amazon.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class ConfigFileReader {
	private Properties properties;
	private final String propertyFilePath= "..\\amazon-app\\src\\main\\resources\\config.properties";
	
	
	 public ConfigFileReader(){
		 BufferedReader reader;
		 try {
			 reader = new BufferedReader(new FileReader(propertyFilePath));
			 properties = new Properties();
			 try {
				 properties.load(reader);
				 reader.close();
			 } catch (IOException e) {
				 e.printStackTrace();
			 }
		 } catch (FileNotFoundException e) {
			 e.printStackTrace();
			throw new RuntimeException("Configuration.properties not found at " + propertyFilePath);
		 } 
	 }
	 
	 public String getURL() { 
		 String URL = properties.getProperty("URL");
		 if(URL != null) return URL;
		 else throw new RuntimeException("URL is not specified in the config.properties file."); 
	 }
	 public String getUserName() { 
		 String USER_NAME = properties.getProperty("USER_NAME");
		 if(USER_NAME != null) return USER_NAME;
		 else throw new RuntimeException("Username is not specified in the config.properties file."); 
	 }
	 public String getPassword() { 
		 String PASSWORD = properties.getProperty("PASSWORD");
		 if(PASSWORD != null) return PASSWORD;
		 else throw new RuntimeException("Password is not specified in the config.properties file."); 
	 }
	 public String[] getShoppingList() throws Exception { 
		 String[] ShoppingList = new String[2];
		 ExcelFileReader readData = new ExcelFileReader();
		 ShoppingList[0] = readData.getLaptop();
		 ShoppingList[1] =  readData.getKeyboard();
		 if(ShoppingList != null) return ShoppingList;
		 else throw new RuntimeException("Product is not specified in the excel sheet."); 
	 }

}
