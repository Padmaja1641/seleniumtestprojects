package com.intimetec.amazon.util;

import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LaunchBrowser {
	private static final Logger logger = Logger.getLogger(LaunchBrowser.class);
	private static ConfigFileReader config = null;
	static {
		config = new ConfigFileReader();
	}
	
	public static WebDriver launchChromeBrowser() {
		logger.info("Invoked launchChromeBrowser");
		System.setProperty("webdriver.chrome.driver", "./ChromeDriver/chromedriver.exe");
		ConfigFileReader config = new ConfigFileReader();
		WebDriver driver = new ChromeDriver();
		logger.info("ChromeBrowser is successfully launched.");
		driver.get(config.getURL());
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}

	public static ConfigFileReader getConfig() {
		return config;
	}

}
	